from django.http import request
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from mata_kuliah.forms import FormMataKuliah
from mata_kuliah.models import MataKuliah

# Create your views here.

def tambahMataKuliah(request):
    if request.POST:
        form = FormMataKuliah(request.POST)
        if form.is_valid:
            form.save()
            form = FormMataKuliah()
            berhasil = "Mata kuliah Berhasil tersimpan"
            MataKuliahs = MataKuliah.objects.all()
            isian = {
                "form" : form,
                "berhasil" : berhasil,
                'matakuliahs' : MataKuliahs
            }
            return render(request, "mata_kuliah/tambah.html", isian)
            # return HttpResponseRedirect('/tambahmatakuliah/')
    else:
        form = FormMataKuliah()
        MataKuliahs = MataKuliah.objects.all()
        isian = {
            "form" : form,
            'matakuliahs' : MataKuliahs
        }        
    
        return render(request, 'mata_kuliah/tambah.html', isian)

def lihatMataKuliah(request):
    MataKuliahs = MataKuliah.objects.all()
    isian = {
        'matakuliahs' : MataKuliahs
    }
    return render(request, 'mata_kuliah/lihat.html', isian)

def hapusMatkul(request, id_matkul):
    matkul = MataKuliah.objects.filter(id=id_matkul)
    matkul.delete()
    MataKuliahs = MataKuliah.objects.all()
    form = FormMataKuliah()
    isian = {
        "form" : form,
        'matakuliahs' : MataKuliahs
    }        
    
    return render(request, 'mata_kuliah/tambah.html', isian)
    
    #return redirect(tambahMataKuliah)