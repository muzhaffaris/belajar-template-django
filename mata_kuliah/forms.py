from django.forms import ModelForm
from django.forms import fields
from django.forms import widgets
from mata_kuliah.models import MataKuliah
from django import forms

class FormMataKuliah(ModelForm):
    class Meta:
        model = MataKuliah
        fields = "__all__"

        widgets = {
            'nama' : forms.TextInput({'class':'form-control'}),
            'dosen' : forms.TextInput({'class':'form-control'}),
            'jumlahSKS' : forms.NumberInput({'class':'form-control'}),
            'deskripsi' : forms.Textarea({'class':'form-control'}),
            'semesterTahun' : forms.TextInput({'class':'form-control'}),
            'kelas' : forms.TextInput({'class':'form-control'}),
        }