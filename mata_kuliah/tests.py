from django.http import response
from django.http import request
from django.http.request import HttpRequest
from django.test import TestCase
from mata_kuliah.views import tambahMataKuliah

# Create your tests here.
class MataKuliahTest(TestCase):

    def test_tambahmatakuliah_url_status(self):
        response = self.client.get('/tambahmatakuliah/')
        self.assertEqual(response.status_code, 200)

    def test_tambahmatakuliah_html(self):
        response = self.client.get('/tambahmatakuliah/')
        self.assertTemplateUsed(response, 'mata_kuliah/tambah.html')

    def test_konten(self):
        request = HttpRequest()
        response = tambahMataKuliah(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Tambah Mata Kuliah', html_response)
