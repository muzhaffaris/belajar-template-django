from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=500)
    dosen = models.CharField(max_length=500)
    jumlahSKS = models.IntegerField(null=True)
    deskripsi = models.TextField()
    semesterTahun = models.CharField(max_length=25)
    kelas = models.CharField(max_length=25)

    def __str__(self):
        return self.nama