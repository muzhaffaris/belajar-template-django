from mata_kuliah.views import lihatMataKuliah
from django.urls import path

from . import views

app_name = 'mata_kuliah'

urlpatterns = [
    path('tambahmatakuliah/', views.tambahMataKuliah, name="tambahMataKuliah"),
    path('lihatmatakuliah/', views.lihatMataKuliah, name='lihatMataKuliah'),
    path('tambahmatakuliah/hapus/<int:id_matkul>', views.hapusMatkul, name='hapusMatkul'),
]