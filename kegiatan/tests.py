from django.http import response
from belajar_templat_django.settings import TEMPLATES
from django.test import TestCase, LiveServerTestCase
from . import views
from .models import Peserta, Kegiatan
from .views import tambahKegiatan, tambahPeserta, kegiatan
from django.test import Client
from django.urls import resolve, reverse



# Create your tests here.
class ModelTest(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama="budi")
        self.peserta = Peserta.objects.create(nama="labib")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Peserta.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "budi")
        self.assertEqual(str(self.peserta), "labib")

class UrlsTest(TestCase):

    def setUp(self):
        self.kUrl = reverse("kegiatan:kegiatan")

    def test_kegiatan_page_use_right_function(self):
        found = resolve(self.kUrl)
        self.assertEqual(found.func, kegiatan)

    #TODO: tambahin urls test

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.k = Kegiatan.objects.create(nama="Berpesta")
        self.kUrl = reverse("kegiatan:kegiatan")
        self.tambahKUrl = reverse("kegiatan:tambahKegiatan")
        self.tambahPurl = reverse("kegiatan:tambahPeserta", args=[self.k.id])
    
    def test_kegiatan_url_status(self):
        response = self.client.get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_post_tambah_kegiatan(self):
        response = self.client.post(self.tambahKUrl,{
            'nama':'bayu'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')
        self.assertContains(response, "bayu")

    def test_get_tambah_kegiatan(self):
        response = self.client.get(self.tambahKUrl)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kegiatan/tambahKegiatan.html")

    # def test_post_tambah_peserta(self):
    #     response = self.client.post(self.tambahPurl,{
    #         'nama':'binomo'
    #     }, follow=True)
    #     self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')
    #     self.assertContains(response, "binomo")

    # def tambah_peserta_url_status(self):
    #     response = self.client.get(self.tambahPurl)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'kegiatan/tambahPeserta.html')
