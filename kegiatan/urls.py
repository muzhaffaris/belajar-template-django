from main.views import story1
from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('kegiatan/tambah/', views.tambahKegiatan, name='tambahKegiatan'),
    path('kegiatan/tambahPeserta/<int:id_kegiatan>', views.tambahPeserta, name='tambahPeserta'),
]
