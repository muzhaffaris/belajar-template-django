from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=150)
    Kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nama