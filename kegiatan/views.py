from django.forms.formsets import formset_factory
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from kegiatan.forms import FormKegiatan, FormPeserta
from kegiatan.models import Kegiatan, Peserta

# Create your views here.
def kegiatan(request):
    
    kegiatans = Kegiatan.objects.all()
    pesertas = Peserta.objects.all()

    context = {
        "kegiatans" : kegiatans,
        "pesertas":pesertas,
    }
    return render(request, 'kegiatan/kegiatan.html', context)

def tambahKegiatan(request):
    if request.POST:
        form = FormKegiatan(request.POST)
        if form.is_valid:
            form.save()
            # form = FormKegiatan()
            return HttpResponseRedirect('/kegiatan/')
    
    else:
        form = FormKegiatan()
        context = {
            "form" :form,
        }
        return render(request, 'kegiatan/tambahKegiatan.html', context)

def tambahPeserta(request, id_kegiatan):
    kegiatan = Kegiatan.objects.get(id=id_kegiatan)
    pesertaFormSet = inlineformset_factory(Kegiatan, Peserta, FormPeserta)
    formset = pesertaFormSet(instance=kegiatan)
    if request.POST:
        formset = pesertaFormSet(request.POST, instance=kegiatan)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect('/kegiatan/')
    return render(request, 'kegiatan/tambahPeserta.html', {'formset':formset, 'id':id_kegiatan})
