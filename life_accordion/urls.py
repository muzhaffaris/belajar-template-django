from django.urls import path
from . import views

app_name = 'life_accordion'

urlpatterns = [
    path('accordion/', views.accordion, name='accordion'),
]