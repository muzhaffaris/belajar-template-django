from django.apps import AppConfig


class LifeAccordionConfig(AppConfig):
    name = 'life_accordion'
