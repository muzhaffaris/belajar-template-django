from django.http import response
from django.test import TestCase

# Create your tests here.
class viewsTest(TestCase):

    def test_url_status(self):
        response = self.client.get('/accordion/')
        self.assertEqual(response.status_code, 200)