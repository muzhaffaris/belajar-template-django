from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
    path('registration/', views.registration, name="registration"),
    path('login/', views.loginPage, name="login"),
    path('logout/', views.logoutUser, name="logout"),
]