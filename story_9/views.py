from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from .forms import CreateUserForm

# Create your views here.
def registration(request):
    if request.user.is_authenticated:
        return redirect('../')


    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login/')

    context = {
        "form":form,
    }
    return render(request, "registration.html", context)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('../')

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('../')
        else:
            messages.info(request, 'Username atau Password Salah!')

    context = {}
    return render(request, "login.html", context)

def logoutUser(request):
    logout(request)
    return redirect('/login/')