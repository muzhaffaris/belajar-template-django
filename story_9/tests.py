from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.
class ViewsTest(TestCase):

    def test_url_status_code_200(self):
        response_login = self.client.get("/login/")
        response_registration = self.client.get("/registration/")
        
        self.assertEqual(response_login.status_code, 200)
        self.assertEqual(response_registration.status_code, 200)
        

    def test_logout_redirect(self):
        response_logout = self.client.get("/logout/")
        self.assertRedirects(response_logout, '/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_sudah_login(self):
        user = User.objects.create(username='Tester')
        user.set_password('testing12345')
        user.save()

        login = self.client.login(username='Tester', password='testing12345')
        self.assertTrue(login)


    def test_halaman_login_tapi_sudah_login(self):
        user = User.objects.create(username='Tester')
        user.set_password('testing12345')
        user.save()

        self.client.login(username='Tester', password='testing12345')
        response = self.client.get('/login/')
        self.assertRedirects(response, '/', status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_halaman_register_tapi_sudah_login(self):
        user = User.objects.create(username='Tester')
        user.set_password('testing12345')
        user.save()

        self.client.login(username='Tester', password='testing12345')
        response = self.client.get('/registration/')
        self.assertRedirects(response, '/', status_code=302, target_status_code=200, fetch_redirect_response=True)
