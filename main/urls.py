from main.views import story1
from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('kucing/', views.kucing, name='kucing'),
    path('story1/', views.story1, name='story1'),
]
