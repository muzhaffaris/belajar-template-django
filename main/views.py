from django.shortcuts import render


def home(request):
    return render(request, 'main/index.html')

def kucing(request):
    return render(request, 'main/kucing.html')

def story1(request):
    return render(request, 'main/story1.html')
