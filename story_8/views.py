from django.http.response import JsonResponse
from django.shortcuts import render
import requests
import json

# Create your views here.
def story8(request):
    return render(request, 'story8.html')

def dataBuku(request):
    argument = request.GET['q']
    url_api = "https://www.googleapis.com/books/v1/volumes?q=" + argument
    url_request = requests.get(url_api)
    # print(url_request.content)
    data = json.loads(url_request.content)
    return JsonResponse(data, safe=False)