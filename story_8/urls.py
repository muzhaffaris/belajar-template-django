from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    path('story8/', views.story8, name="story8"),
    path('dataBuku/', views.dataBuku, name="dataBuku"),
]