$(document).ready(function(){
    $("#cari").click(function(){
        var judul = $("#inputBuku").val();
        $.ajax({url: "/dataBuku?q=" + judul, success: function(result){
            console.log(result);

            //kosongin div buku dan modals
            $(".buku").empty();
            $("#modals").empty();


            $(".buku").append("<div class=\"table-responsive\"> <table class=\"table\"></table></div>")
            var tableHead = "<thead><tr><th scope=\"col\">#</th><th scope=\"col\">Judul</th><th scope=\"col\">Author</th><th scope=\"col\">Gambar</th><th scope=\"col\">Deskripsi</th></tr></thead>"
            var tableBody = "<tbody class=\"table-body\"></tbody>"
            $(".table").append(tableHead)
            $(".table").append(tableBody)
            var i
            for (i=0; i<result.items.length; i++){

                //ambil deskripsi + buat html modal
                var modalTemplate = "<div class=\"modal fade\" id=\"modal"+ (i+1) +"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\"><div class=\"modal-dialog modal-dialog-centered\" role=\"document\"><div class=\"modal-content\"><div class=\"modal-header\"><h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Deskripsi</h5><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div><div class=\"modal-body\">"+ "Description not found" +"</div></div></div></div>"
                try {
                    var modalTemplate = "<div class=\"modal fade\" id=\"modal"+ (i+1) +"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\"><div class=\"modal-dialog modal-dialog-centered\" role=\"document\"><div class=\"modal-content\"><div class=\"modal-header\"><h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Deskripsi</h5><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div><div class=\"modal-body\">"+ result.items[i].volumeInfo.description +"</div></div></div></div>"
                } catch (error) {
                    
                }

                $(".table-body").append("<tr>")
                $(".table-body").append("<th scope=\"row\">" + (i+1) +"</th>")

                //ambil judul+author
                try {
                    $(".table-body").append("<td>" + result.items[i].volumeInfo.title + "</td>")
                    $(".table-body").append("<td>" + result.items[i].volumeInfo.authors[0] + "</td>")
                } catch (error) {
                    $(".table-body").append("<td>Author: -</td>")
                }

                //ambil link gambar
                try {
                    $(".table-body").append("<td><img src="+ result.items[i].volumeInfo.imageLinks.thumbnail +" width=\"120\" height=\"160\"><br></td>")
                } catch (error) {
                    $(".table-body").append("<td><img src=\"https://i.ibb.co/McfrG8F/noImage.jpg\" alt=\"noImage\"  width=\"120\" height=\"160\"><br></td>")
                }
                
                $(".table-body").append("<td><button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modal"+ (i+1) +"\">Deskripsi</button></td>")

                $(".table-body").append("</tr>")

                //tulis html modal yang sudah diisi dengan deskripsi
                $("#modals").append(modalTemplate)
            }
            
        }});
    });
});